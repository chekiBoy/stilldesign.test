<?php
use Illuminate\Database\Seeder;
use App\Entity\Product;

class ProductsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<25; $i++){
            $product = new Product();
            $product->name = str_random(10);
            $product->sku = str_random().str_pad( mt_rand(1,99999),5,0,1);
            $product->price = mt_rand(100,5000);
            $product->tax = 0.27;
            $product->save();
        }
    }
}
