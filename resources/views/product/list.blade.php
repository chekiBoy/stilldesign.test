@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <h3 class="panel-heading">Termékek</h3>
                   <div class="form-group">
                        <button data-export="true" class="btn btn-info pull-right" type="button"><i class="fa fa-download"></i> Teljes lista exportálás</button>
                    </div>
                     <p class="clearfix"></p>
                	<div class="form-group">
                        <label for="search">Keresés</label>
                        <input class="form-control" type="search" name="search" id="search" placeholder="Termék neve" aria-controls="productList"/>
                    </div>
                 
                   
                    <table id="productList" class="table table-responsive table-hover table-condensed table-stripped">
                    </table>
                   
            </div>
        </div>
    </div>
</div>
@endsection
