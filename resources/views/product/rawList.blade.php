@extends('layouts.rawApp')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <h3 class="panel-heading">Termékek</h3>
                    <table id="productList" class="table table-responsive table-hover table-condensed table-stripped">
                    	<thead>
                    		<tr>
                    			<th>Elnevezés</th>
                    			<th>SKU</th>
                    			<th>Nettó ár</th>
                    			<th>Áfa</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		@foreach($products as $product)
                    			<tr>
                    				<td>{{ $product->name }}</td>
                    				<td>{{ $product->sku }}</td>
                    				<td>{{ (int)$product->price }} Ft</td>
                    				<td>{{ $product->tax*100 }}%</td>
                    			</tr>
                    		@endforeach
                    	</tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
