<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/js/dataTables/css/jquery.dataTables.css') }}" >

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Laravel
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/termeklista') }}">Termékek</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" ></script>
    <script src="{{ asset('assets/js/jquery.autocomplete.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('assets/js/datatables/js/jquery.dataTables.js') }}" type="text/javascript" ></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
		var dataTableOptions = {
				 "columnDefs": [
					           	{'targets': 0,
					             'title': 'Termék neve',
					             'data': 'name',
					             'sortable': true,
					             'render' : function (data, type, row) { 
						             if(data != "")
						             	return  data;
						             else 
							             return "";
					             }
					         	},
					         	{'targets': 1,
						             'title': 'SKU',
						             'data': 'sku',
						             'sortable': true,
						             'class':'hidden-md hidden-sm hidden-xs',
						             'render' : function (data) { 
							             if(data != "")
							             	return data;
							             else 
								             return "";
						             }
						         	},
					         	{'targets': 2,
						             'title': 'Nettó ár',
						             'data': 'price',
						             'sortable': true,
									 'class': '',
						             'render' : function (data) { 
							             if(data != "")
							             	return  parseInt(data) + " Ft";
							             else 
								             return "n/a";
						             }
						         },
					         	{'targets': 3,
						             'title': 'ÁFA',
						             'data': 'tax',
						             'sortable': true,
						             'class':'hidden-xs',
						             'render' : function (data) { 
						            	 if(data != "")
								             	return data*100 + "%";
								             else 
									             return "n/a";
						             }
						         }
					         ]
							,
				"processing": true,
		        "serverSide": true,
		        "ajax": "/productlist/tojson",
		        "responsive": true,
		        "autoWidth": false,
		       "language": {
		    		   "emptyTable":     "Nincs rendelkezésre Álló adat",
	    			   "info":           "Találatok: _START_ - _END_ összesen: _TOTAL_",
	    			   "infoEmpty":      "Nulla találat",
	    			   "infoFiltered":   "(_MAX_ összes rekord közül szűrve)",
	    			   "infoPostFix":    "",
	    			   "infoThousands":  " ",
	    			   "lengthMenu":     "_MENU_ találat oldalanként",
	    			   "loadingRecords": "Betöltés...",
	    			   "processing":     "Feldolgozás...",
	    			   "search":         "Keresés:",
	    			   "zeroRecords":    "Nincs a keresésnek megfelelő találat",
	    			   "paginate": {
	    			       "first":    "Első",
	    			       "previous": "Előző",
	    			       "next":     "Következő",
	    			       "last":     "Utolsó"
	    			   },
	    			   "aria": {
	    			       "sortAscending":  ": aktiválja a növekvő rendezéshez",
	    			       "sortDescending": ": aktiválja a csökkenő rendezéshez"
	    			   },
		        },
		        "dom": '<"top"l>rt<"bottom"ip><"clear">'
        };
    
		var dataTable = $('#productList').dataTable(dataTableOptions);
    
		var dataList = [];
		$("#productList tr td:first-child").each(function(){
			dataList.push({value:$(this).text()});
		});
    	$('[type="search"]').autocomplete({
    		serviceUrl: '/autocomplete/products',
    	    onSelect: function (suggestion) {
    	    	dataTable.fnFilter($('[type="search"]').val());
    	    }
        });

    	$('[data-export]').on('click', function(){
        	location.href="/export";
    	})

        </script>
</body>
</html>
