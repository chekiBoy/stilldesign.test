<?php

use Monolog\Handler\GroupHandler;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/termeklista', 'ProductController@index');

});

Route::any('/productlist/tojson', 'ProductController@getListJson');
Route::any('/autocomplete/products', 'ProductController@getAutoCompleteListJson');
Route::any('/export', 'ProductController@export');
