<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repository\ProductRepository;
use Dompdf\Dompdf;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view("product/list");
    }
    
    public function getListJson(Request $request){
        $repository = new ProductRepository($request);
        $data = $repository->getTableData();
        
        
        return response()->json([
            'recordsTotal' => $repository->countAll(),
            'recordsFiltered' => $repository->getTableDataCount(),
            'data' => $data
        ]);
    }

    public function getAutoCompleteListJson(Request $request){
        $list = with(new ProductRepository($request))->getAutoComplete();
        $suggestion = [];
        foreach ($list as $product){
            $suggestion[] = ['value' => $product->name] ;
        }
        return response()->json(["suggestions" => $suggestion]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $repository = new ProductRepository($request);
        $products = $repository->getTableData();
        $pdf = new Dompdf();
        $pdf->loadHtml((string)view("product/rawlist",compact("products")));
        
        // (Optional) Setup the paper size and orientation
        $pdf->setPaper('A4');
        
        // Render the HTML as PDF
        $pdf->render();
        
        // Output the generated PDF to Browser
        $pdf->stream('termekek_'.date('YmdHi').'.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request            
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id            
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id            
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request            
     * @param int $id            
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id            
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
