<?php
namespace App\Repository;

use App\Entity\Product;
use Illuminate\Http\Request;

class ProductRepository
{

    private $model;
    private $request;
    private $requestArray;
    private $fields = [
        'name',
        'sku',
        'price',
        'tax'
    ];

    public function __construct(Request $request)
    {
        $this->model = new Product();
        $this->request = $request;
        $this->requestArray = $request->all();
    }

    public static function all($columns = ['*'])
    {
        return Product::all($columns);
    }
    
    public function getAutoComplete(){
        return with(new Product)->where('name','like','%'.$this->request->get('query').'%')->get();
    }

    public function getList()
    {
        $where = $this->request->get("search", false);
        $orderBy = $this->request->get("order", "name");
        $direction = $this->request->get("direction", "ASC");
        if ($where !== false) {
            return $this->_listWhere($where, $orderBy, $direction);
        } else {
            return $this->list($orderBy, $direction);
        }
    }

    private function _listWhere($where, $orderBy = "name", $direction = "ASC")
    {
        return $this->model->where("name", "like", $where)
            ->order($orderBy, $direction)
            ->get();
    }

    private function _list($orderBy = "name", $direction = "ASC")
    {
        return $this->model->order($orderBy, $direction)->get();
    }

    public function getTableData()
    {
       
        $q = $this->_getDataTableCrits();
        if(isset($this->requestArray['start'])){
            $q->skip($this->requestArray['start'])->take($this->requestArray['length']);
        }
        
        return $q->get();
    }

    public function getTableDataCount($req = [])
    {
        
        $q = $this->_getDataTableCrits();
        
        return $q->get()->count();
    }
    
    private function _getDataTableCrits(){
        
        $q = $this->model->select(\DB::raw(implode(',', $this->fields)));
        
        $q->where(function ($query) {
            if (isset($this->requestArray['columns'])) {
                for ($i = 0; $i < count($this->requestArray['columns']); $i ++) {
                    $c = $this->requestArray['columns'][$i];
                    if (trim($c['search']['value']) != '' && ($i == 3 || $i == 4)) {
                        $query->where($this->fields[$i], '=', $c['search']['value']);
                    } else
                        if (trim($c['search']['value']) != '') {
                            $query->where($this->fields[$i], 'like', '%' . $c['search']['value'] . '%');
                        }
                }
            }
        });
        
        if (isset($this->requestArray['search']) && trim($this->requestArray['search']['value']) != '') {
            $q->where('name', 'like', '%' . $this->requestArray['search']['value'] . '%');
        }
    
        if (! isset($this->requestArray['order'])) {
            $req['order'][0]['column'] = 0;
            $req['order'][0]['dir'] = 'ASC';
            $req['start'] = 0;
            $req['length'] = 10;
        }
        if(isset($this->requestArray['order'])){
            $q->orderBy($this->requestArray['order'][0]['column'] == 3 ? 'description_count' : $this->fields[$this->requestArray['order'][0]['column']], $this->requestArray['order'][0]['dir']);
        }
        
        return $q;
    }

    public function countAll()
    {
        return $this->model->select([
            'id'
        ])
            ->get()
            ->count();
    }
}
