<?php
namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = "products";

    protected $fillable = [
        "name",
        "sku",
        "price",
        "tax"
    ];
}
